/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.interfaceproject;

/**
 *
 * @author focus
 */
public class Car extends Vehicle implements Runable {

    public Car(String engine) {
        super(engine);
        System.out.println("Create : Car");
        System.out.println("Engine : " + engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Car : startEngine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car : stopEngine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car : raiseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car : break");
    }

    @Override
    public void run() {
        System.out.println("Car : run");
    }
    
}
