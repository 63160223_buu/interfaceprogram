/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.interfaceproject;

/**
 *
 * @author focus
 */
public class Plane extends Vehicle implements Flyable, Runable {

    public Plane(String engine) {
        super(engine);
        System.out.println("Create : Plane");
        System.out.println(engine);
        System.out.println("");
    }

    @Override
    public void startEngine() {
        System.out.println("Plane : Start engine");
    }

    @Override
    public void stopEngine() {

    }

    @Override
    public void raiseSpeed() {

    }

    @Override
    public void applyBreak() {

    }

    @Override
    public void fly() {
        System.out.println("Plane : fly");
    }

    @Override
    public void run() {
         System.out.println("Plane : run");
    }

}
