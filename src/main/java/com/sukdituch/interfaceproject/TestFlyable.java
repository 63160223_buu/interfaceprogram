/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.interfaceproject;

/**
 *
 * @author focus
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat();
        Plane plane = new Plane("Engine I");
        Dog dog = new Dog("Dang");
        Car car = new Car("Engine Drive");

        bat.fly(); // เป็น Animal, Poultry, Flyable
        plane.fly(); // เป็นVehicle, Flyable

        // เราสามารถจัดกลุ่ม2ตัวนี้ได้นั่นก็คือ ซึ่งเมื่อเรา . จะมองเห็นได้แค่ .fly เนื่องจาก เรามองว่ามันเป็นจำพวก Flyable
        Flyable[]flyable = {bat, plane};
        for(Flyable f : flyable) { // : คือ in (อยู่ใน)
            //เชค Plane ว่าก่อน fly ต้องทำอะไรก่อน
            if(f instanceof Plane){ // กำหนดให้ f เป็น plane
            Plane p = (Plane)f; // ทำการ crash p ให้มอง ตัว f ว่าเป็น Plane
            //ทีนี้เราก็จะสามารถทำให้ p เรียกใช้ข้อมูลของ Plane ได้แล้ว
            p.startEngine();
            p.run();
            }
            
            f.fly();
        }
        
        Runable[]runable = {dog,plane,car};
        for(Runable r : runable){
            if(r instanceof Car){
            Car c = (Car)r;
            
            c.startEngine();
            c.raiseSpeed();
            c.applyBreak();
            c.stopEngine();
        }
            r.run();
        }
    }
}
