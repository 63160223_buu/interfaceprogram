/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.interfaceproject;

/**
 *
 * @author focus
 */
public abstract class Vehicle {

    private String engine;

    public Vehicle(String engine) {
        this.engine = engine;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public abstract void startEngine();

    public abstract void stopEngine();

    public abstract void raiseSpeed();

    public abstract void applyBreak();
}
